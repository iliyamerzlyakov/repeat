package task8;
import java.util.Scanner;
public class task8 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        if (column == line) {
            System.out.println("Матрица должна быть прямоугольной");
        } else {
            int[][] a = new int[line][column];
            for (int i = 0; i < line; i++) {
                for (int j = 0; j < column; j++) {
                    a[i][j] = (int) (Math.random() * 10);
                }
            }
            for (int i = 0; i < line; i++, System.out.println()) {
                for (int j = 0; j < column; j++) {
                    System.out.print(a[i][j] + " ");
                }
            }
            matrix(a);
            for (int i = 0; i < line; i++, System.out.println()) {
                for (int j = 0; j < column; j++) {
                    System.out.print(a[i][j] + " ");
                }
            }
        }
    }

    private static void matrix(int[][] a) {
        System.out.print("Число для обнуления: ");
        int number = scanner.nextInt();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (number == a[i][j]) {
                    a[0][j] = a[i][j];
                    for (int k = 0; k < a.length; k++) {
                        a[0 + k][j] = 0;
                    }
                }
            }
        }
    }
}
