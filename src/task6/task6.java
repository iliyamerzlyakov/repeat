package task6;
import java.util.Scanner;
public class task6 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите число для получения таблицы умножения : ");
        int x = scanner.nextInt();
        System.out.println(x+" * 2 = "+x*2);
        System.out.println(x+" * 3 = "+x*3);
        System.out.println(x+" * 4 = "+x*4);
        System.out.println(x+" * 5 = "+x*5);
        System.out.println(x+" * 6 = "+x*6);
        System.out.println(x+" * 7 = "+x*7);
        System.out.println(x+" * 8 = "+x*8);
        System.out.println(x+" * 9 = "+x*9);
        System.out.println(x+" * 10 = "+x*10);

    }
}
