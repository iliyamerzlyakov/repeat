package task11;
import java.util.Scanner;
public class task11 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите количество суток : ");
        int n = scanner.nextInt();
        System.out.println("В этом количестве суток "+n*24+" часов, "+n*24*60+" минут и "+n*24*60*60+" секунд");
    }
}
